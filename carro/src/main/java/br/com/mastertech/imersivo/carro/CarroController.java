package br.com.mastertech.imersivo.carro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CarroController {
	
	@Autowired
	private CarroService carroService;

	@GetMapping("/carro/{modelo}")
	public Carro novoCarro(@PathVariable String modelo) {
		return carroService.novoCarro(modelo);
	}
}
